using NUnit.Framework;
using Otus.CreationalPatterns;

namespace TestProject
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test_GetHumanClone()
        {
            var program = new Program();
            var human = new Human
            {
                Name = "Oleg",
                Age = 34,
                Body = new Body
                {
                    Height = 180,
                    Weight = 85,
                    SkinColor = SkinColor.White
                }
            };
            var clone = program.GetHumanClone(human);

            Assert.AreEqual(human.Name, clone.Name);
            Assert.AreEqual(human.ID, clone.ID);
            Assert.AreEqual(human.Age, clone.Age);
            Assert.AreEqual(human.Body.ID, clone.Body.ID);
            Assert.AreEqual(human.Body.SkinColor, clone.Body.SkinColor);
            Assert.AreEqual(human.Body.Height, clone.Body.Height);
            Assert.AreEqual(human.Body.Weight, clone.Body.Weight);
        }
    }
}