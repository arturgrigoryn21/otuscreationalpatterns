﻿using System;

namespace Otus.CreationalPatterns
{
    class Program
    {
        static void Main(string[] args)
        {

        }

        public Human GetHumanClone(Human human)
        {
            return human.Clone() as Human;
        }
    }
}
