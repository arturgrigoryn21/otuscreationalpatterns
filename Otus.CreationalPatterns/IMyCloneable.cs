﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.CreationalPatterns
{
    interface IMyCloneable<T>
    {
        T Clone();
    }
}
