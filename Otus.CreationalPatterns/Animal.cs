﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.CreationalPatterns
{
    //Базовый клонируемый класс
    abstract class Animal : IMyCloneable<Animal>
    {
        public int Age { get; set; }
        public Body Body { get; set; }
        public Animal() { }

        protected Animal(Animal animal)
        {
            Age = animal.Age;
            Body = animal.Body.Clone();
        }
        abstract public Animal Clone();
    }
}
