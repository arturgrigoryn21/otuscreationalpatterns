﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.CreationalPatterns
{
    //Клонируемая часть клонируемого элемента
    class Body : IMyCloneable<Body>, ICloneable
    {
        public Guid ID { get; set; }
        public double Height { get; set; }
        public double Weight { get; set; }
        public SkinColor SkinColor { get; set; }

        public Body()
        {
            ID = Guid.NewGuid();
        }

        public Body(Body body)
        {
            Height = body.Height;
            Weight = body.Weight;
            SkinColor = body.SkinColor;
            ID = body.ID;
        }
        public Body Clone()
            => new Body(this);


        object ICloneable.Clone() 
            => MemberwiseClone();

    }
}
