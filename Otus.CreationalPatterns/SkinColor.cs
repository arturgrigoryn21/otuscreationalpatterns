﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.CreationalPatterns
{
    public enum SkinColor
    {
        White,
        Black,
        Yellow,
        Red
    }
}
