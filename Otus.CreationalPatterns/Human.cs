﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.CreationalPatterns
{
    //Наследник кланируемого класса
    class Human : Animal, ICloneable
    {
        public string Name { get; set; }       
        
        public Guid ID { get; private set; }

        private Human(Human human) : base(human)
        {
            Name = human.Name;
            ID = human.ID;
        }
        public Human()
        {
            ID = Guid.NewGuid();
        }
        public override Animal Clone()
        {
            return new Human(this);
        }

        object ICloneable.Clone()
        {
            return new Human
            {
                Name = Name,
                ID = ID,
                Age = Age, 
                Body = Body.Clone()
            };
        }
    }
}
